package com.example.platzi_devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlatziDevopsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlatziDevopsApplication.class, args);
    }

}
